# PatientSky Challange

This is frontend candidate assignment and should be consider confidential as part of an interview process

## Stack

- Typescript 2.9
- React 16
- React Router 4
- Webpack 4
- Less

## Install

Gloabal dependencies

    npm i typescript -g

Install dependencies

    npm i

## Run it

    npm run serve


To do a production build and deploy:

    npm run build-prod

## Notes

- Static files are served from the `www/public` folder (has been left in the repo on purpose).
- Live reload sense changes on the server and client.
- I did not write less because I felt was not necessary, and I prefered to focus on Server side rendering and webpack configuration

## Questions?

Hit me up at pm.calabrese@gmail.com or visit pmcalabrese.com.