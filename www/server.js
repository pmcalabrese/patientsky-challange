(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(global, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/components/PatientAddForm.tsx":
/*!*******************************************!*\
  !*** ./src/components/PatientAddForm.tsx ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\n};\nObject.defineProperty(exports, \"__esModule\", { value: true });\n/* tslint:disable:no-unused-variable */\nconst React = __webpack_require__(/*! react */ \"react\");\n/* tslint:disable:no-unused-variable */\nconst react_1 = __webpack_require__(/*! react */ \"react\");\nconst autobind_decorator_1 = __webpack_require__(/*! autobind-decorator */ \"autobind-decorator\");\nclass PatientAddForm extends react_1.Component {\n    constructor() {\n        super(...arguments);\n        this.inputs = {\n            $name_input: null,\n            $email_input: null,\n            $birthday_input: null,\n            $phone_input: null\n        };\n    }\n    componentDidMount() {\n        this.inputs.$name_input.focus();\n        if (this.props.initPatient) {\n            this.setState({\n                patient: this.props.initPatient\n            });\n        }\n    }\n    componentWillReceiveProps(props) {\n        if (props.initPatient) {\n            for (const input in this.inputs) {\n                if (this.inputs.hasOwnProperty(input)) {\n                    const $input = this.inputs[input];\n                    $input.value = props.initPatient[$input.name];\n                }\n            }\n            this.setState({\n                edit_mode: true\n            });\n        }\n    }\n    addPatient(e) {\n        e.preventDefault();\n        this.setState({\n            patient: {\n                name: this.inputs.$name_input.value,\n                email: this.inputs.$email_input.value,\n                birthday: this.inputs.$birthday_input.value,\n                phone: this.inputs.$phone_input.value\n            }\n        }, () => {\n            if (this.props.onAddPatient)\n                this.props.onAddPatient(this.state.patient);\n            for (const input in this.inputs) {\n                if (this.inputs.hasOwnProperty(input)) {\n                    const $input = this.inputs[input];\n                    $input.value = \"\";\n                }\n            }\n        });\n        if (this.state.edit_mode) {\n            this.setState({\n                edit_mode: false\n            });\n        }\n    }\n    handleInputChange(e) {\n    }\n    render() {\n        return React.createElement(\"form\", { onSubmit: this.addPatient },\n            React.createElement(\"label\", { htmlFor: \"\" },\n                \"Name:\",\n                React.createElement(\"input\", { required: true, name: \"name\", ref: e => this.inputs.$name_input = e, type: \"text\", onInput: this.handleInputChange })),\n            React.createElement(\"br\", null),\n            React.createElement(\"label\", { htmlFor: \"\" },\n                \"Email:\",\n                React.createElement(\"input\", { required: true, name: \"email\", ref: e => this.inputs.$email_input = e, type: \"email\", onInput: this.handleInputChange })),\n            React.createElement(\"br\", null),\n            React.createElement(\"label\", { htmlFor: \"\" },\n                \"Birthday:\",\n                React.createElement(\"input\", { required: true, name: \"birthday\", ref: e => this.inputs.$birthday_input = e, type: \"text\", onInput: this.handleInputChange })),\n            React.createElement(\"br\", null),\n            React.createElement(\"label\", { htmlFor: \"\" },\n                \"Phone:\",\n                React.createElement(\"input\", { required: true, name: \"phone\", ref: e => this.inputs.$phone_input = e, type: \"text\", onInput: this.handleInputChange })),\n            React.createElement(\"br\", null),\n            React.createElement(\"button\", { type: \"submit\" },\n                (this.state && this.state.edit_mode) ? \"Save\" : \"Create\",\n                \" patient\"));\n    }\n}\n__decorate([\n    autobind_decorator_1.default\n], PatientAddForm.prototype, \"addPatient\", null);\n__decorate([\n    autobind_decorator_1.default\n], PatientAddForm.prototype, \"handleInputChange\", null);\nexports.PatientAddForm = PatientAddForm;\n\n\n//# sourceURL=webpack:///./src/components/PatientAddForm.tsx?");

/***/ }),

/***/ "./src/components/PatientList.tsx":
/*!****************************************!*\
  !*** ./src/components/PatientList.tsx ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst React = __webpack_require__(/*! react */ \"react\");\nconst react_1 = __webpack_require__(/*! react */ \"react\");\nconst utils_1 = __webpack_require__(/*! ../utils */ \"./src/utils.ts\");\nclass PatientList extends react_1.Component {\n    render() {\n        {\n            return this.props.patients.map((patient) => {\n                return React.createElement(\"li\", { key: patient.email },\n                    React.createElement(\"span\", null,\n                        patient.name,\n                        \" \"),\n                    React.createElement(\"span\", null,\n                        React.createElement(\"a\", { href: `mailto:${patient.email}` }, patient.email),\n                        \" \"),\n                    React.createElement(\"span\", null,\n                        patient.phone,\n                        \" \"),\n                    React.createElement(\"span\", null,\n                        utils_1.formatBirthday(patient.birthday),\n                        \" \"),\n                    React.createElement(\"button\", { type: \"button\", onClick: e => this.props.onEdit(patient.email) }, \"edit\"),\n                    React.createElement(\"button\", { type: \"button\", onClick: e => this.props.onDelete(patient.email) }, \"delete\"));\n            });\n        }\n    }\n}\nexports.PatientList = PatientList;\n\n\n//# sourceURL=webpack:///./src/components/PatientList.tsx?");

/***/ }),

/***/ "./src/components/SearchMedicine.tsx":
/*!*******************************************!*\
  !*** ./src/components/SearchMedicine.tsx ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\n};\nObject.defineProperty(exports, \"__esModule\", { value: true });\n/* tslint:disable:no-unused-variable */\nconst React = __webpack_require__(/*! react */ \"react\");\n/* tslint:disable:no-unused-variable */\nconst react_1 = __webpack_require__(/*! react */ \"react\");\nconst patientsky_http_1 = __webpack_require__(/*! ../services/patientsky.http */ \"./src/services/patientsky.http.ts\");\nconst autobind_decorator_1 = __webpack_require__(/*! autobind-decorator */ \"autobind-decorator\");\nclass SearchMedicine extends react_1.Component {\n    constructor() {\n        super(...arguments);\n        this.$input = null;\n    }\n    componentWillMount() {\n        this.setState({\n            results: []\n        });\n    }\n    search() {\n        if (this.$input.value === \"\") {\n            this.setState({\n                results: []\n            });\n        }\n        else {\n            patientsky_http_1.getMedicine(this.$input.value).then((data) => {\n                if (!data)\n                    return;\n                this.setState({\n                    results: data\n                });\n            });\n        }\n    }\n    render() {\n        // if (!this.state) return <span></span>\n        return React.createElement(\"div\", null,\n            React.createElement(\"input\", { type: \"text\", ref: e => this.$input = e, onChange: this.search }),\n            React.createElement(\"ul\", null, this.state.results.map((result) => {\n                return React.createElement(\"li\", { key: result.id }, result.productName);\n            })));\n    }\n}\n__decorate([\n    autobind_decorator_1.default\n], SearchMedicine.prototype, \"search\", null);\nexports.SearchMedicine = SearchMedicine;\n\n\n//# sourceURL=webpack:///./src/components/SearchMedicine.tsx?");

/***/ }),

/***/ "./src/const.ts":
/*!**********************!*\
  !*** ./src/const.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n/**\n * Contains constants and interfaces\n */\nObject.defineProperty(exports, \"__esModule\", { value: true });\nexports.patients = [\n    {\n        name: \"Marco\",\n        email: \"pm.calabrese@gmail.com\",\n        birthday: \"12021986\",\n        phone: \"+4542838188\"\n    },\n    {\n        name: \"Gianlu\",\n        email: \"gia@gmail.com\",\n        birthday: \"05031986\",\n        phone: \"+454233181\"\n    },\n    {\n        name: \"Frank\",\n        email: \"frank@gmail.com\",\n        birthday: \"10011985\",\n        phone: \"+4542238122\"\n    },\n    {\n        name: \"Gianvito\",\n        email: \"gi@gmail.com\",\n        birthday: \"03091992\",\n        phone: \"+4512348189\"\n    }\n];\n\n\n//# sourceURL=webpack:///./src/const.ts?");

/***/ }),

/***/ "./src/pages/ListPage.tsx":
/*!********************************!*\
  !*** ./src/pages/ListPage.tsx ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nvar __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {\n    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;\n    if (typeof Reflect === \"object\" && typeof Reflect.decorate === \"function\") r = Reflect.decorate(decorators, target, key, desc);\n    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;\n    return c > 3 && r && Object.defineProperty(target, key, r), r;\n};\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst React = __webpack_require__(/*! react */ \"react\");\nconst react_1 = __webpack_require__(/*! react */ \"react\");\nconst autobind_decorator_1 = __webpack_require__(/*! autobind-decorator */ \"autobind-decorator\");\nconst PatientList_1 = __webpack_require__(/*! ../components/PatientList */ \"./src/components/PatientList.tsx\");\nconst PatientAddForm_1 = __webpack_require__(/*! ../components/PatientAddForm */ \"./src/components/PatientAddForm.tsx\");\nconst const_1 = __webpack_require__(/*! ../const */ \"./src/const.ts\");\nconst utils_1 = __webpack_require__(/*! ../utils */ \"./src/utils.ts\");\nclass ListPage extends react_1.Component {\n    componentDidMount() {\n        this.setState({\n            patients: const_1.patients,\n            edit_patient: null\n        });\n    }\n    addPatient(patient) {\n        let { i } = utils_1.findWhere(this.state.patients, { email: patient.email });\n        if (i === -1) { // ADD\n            this.state.patients.push(patient);\n        }\n        else {\n            this.state.patients[i] = patient;\n        }\n        this.setState({\n            patients: this.state.patients,\n            edit_patient: null\n        });\n    }\n    onEdit(email) {\n        let { object } = utils_1.findWhere(this.state.patients, { email: email });\n        console.log('patient', object);\n        this.setState({\n            edit_patient: object\n        });\n    }\n    onDelete(email) {\n        let { i } = utils_1.findWhere(this.state.patients, { email: email });\n        this.state.patients.splice(i, 1);\n        this.setState({\n            patients: this.state.patients\n        });\n    }\n    render() {\n        return React.createElement(\"div\", null,\n            React.createElement(\"h2\", null, \"Patient List\"),\n            React.createElement(\"section\", null, (this.state && this.state.patients) ? React.createElement(PatientList_1.PatientList, { patients: this.state.patients, onEdit: this.onEdit, onDelete: this.onDelete }) : null),\n            React.createElement(\"h3\", null, \"Add Patient\"),\n            React.createElement(\"section\", null,\n                React.createElement(PatientAddForm_1.PatientAddForm, { onAddPatient: this.addPatient, initPatient: (this.state && this.state.edit_patient) ? this.state.edit_patient : null })));\n    }\n}\n__decorate([\n    autobind_decorator_1.default\n], ListPage.prototype, \"addPatient\", null);\n__decorate([\n    autobind_decorator_1.default\n], ListPage.prototype, \"onEdit\", null);\n__decorate([\n    autobind_decorator_1.default\n], ListPage.prototype, \"onDelete\", null);\nexports.ListPage = ListPage;\n\n\n//# sourceURL=webpack:///./src/pages/ListPage.tsx?");

/***/ }),

/***/ "./src/pages/MainPage.tsx":
/*!********************************!*\
  !*** ./src/pages/MainPage.tsx ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst React = __webpack_require__(/*! react */ \"react\");\nconst env = \"development\" || 'development';\nclass MainPage extends React.Component {\n    render() {\n        // Add helmet to control title at the view level\n        // const helmet = Helmet.rewind();\n        return (React.createElement(\"html\", { lang: \"en-us\" },\n            React.createElement(\"head\", null,\n                React.createElement(\"meta\", { charSet: \"utf-8\" }),\n                React.createElement(\"meta\", { name: \"viewport\", content: \"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0\" }),\n                React.createElement(\"title\", null, \"Home\"),\n                env === \"development\" ? React.createElement(\"script\", { src: \"http://localhost:35729/livereload.js?snipver=1\" }) : null,\n                React.createElement(\"link\", { href: \"/style.css\", rel: \"stylesheet\" }),\n                React.createElement(\"link\", { href: \"/favicon.ico\" })),\n            React.createElement(\"body\", null,\n                React.createElement(\"nav\", null,\n                    React.createElement(\"ul\", null,\n                        React.createElement(\"li\", null,\n                            React.createElement(\"a\", { href: \"/\" }, \"Patient List\")),\n                        React.createElement(\"li\", null,\n                            React.createElement(\"a\", { href: \"/search\" }, \"Medicine Search\")))),\n                React.createElement(\"div\", { id: \"body\", dangerouslySetInnerHTML: { __html: this.props.content } }),\n                React.createElement(\"script\", { src: \"/app.js\" }))));\n    }\n}\nexports.default = MainPage;\n\n\n//# sourceURL=webpack:///./src/pages/MainPage.tsx?");

/***/ }),

/***/ "./src/pages/SearchMedicinePage.tsx":
/*!******************************************!*\
  !*** ./src/pages/SearchMedicinePage.tsx ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\n/* tslint:disable:no-unused-variable */\nconst React = __webpack_require__(/*! react */ \"react\");\n/* tslint:disable:no-unused-variable */\nconst react_1 = __webpack_require__(/*! react */ \"react\");\nconst SearchMedicine_1 = __webpack_require__(/*! ../components/SearchMedicine */ \"./src/components/SearchMedicine.tsx\");\nclass SearchMedicinePage extends react_1.Component {\n    render() {\n        return React.createElement(\"div\", null,\n            React.createElement(\"h2\", null, \"Search Medicine\"),\n            React.createElement(SearchMedicine_1.SearchMedicine, null));\n    }\n}\nexports.SearchMedicinePage = SearchMedicinePage;\n\n\n//# sourceURL=webpack:///./src/pages/SearchMedicinePage.tsx?");

/***/ }),

/***/ "./src/routes.tsx":
/*!************************!*\
  !*** ./src/routes.tsx ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\n// import \"./less/style.less\";\nconst React = __webpack_require__(/*! react */ \"react\");\nconst react_router_dom_1 = __webpack_require__(/*! react-router-dom */ \"react-router-dom\");\n/**\n * IMPORT PAGES\n */\nconst ListPage_1 = __webpack_require__(/*! ./pages/ListPage */ \"./src/pages/ListPage.tsx\");\nconst SearchMedicinePage_1 = __webpack_require__(/*! ./pages/SearchMedicinePage */ \"./src/pages/SearchMedicinePage.tsx\");\nexports.Routes = () => (React.createElement(react_router_dom_1.Switch, null,\n    React.createElement(react_router_dom_1.Route, { path: \"/\", exact: true, component: ListPage_1.ListPage }),\n    React.createElement(react_router_dom_1.Route, { path: \"/search\", exact: true, component: SearchMedicinePage_1.SearchMedicinePage })));\n\n\n//# sourceURL=webpack:///./src/routes.tsx?");

/***/ }),

/***/ "./src/server.tsx":
/*!************************!*\
  !*** ./src/server.tsx ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst express = __webpack_require__(/*! express */ \"express\");\nconst errorHandler = __webpack_require__(/*! errorhandler */ \"errorhandler\");\nconst http = __webpack_require__(/*! http */ \"http\");\nconst path = __webpack_require__(/*! path */ \"path\");\nconst React = __webpack_require__(/*! react */ \"react\");\nconst ReactDOMServer = __webpack_require__(/*! react-dom/server */ \"react-dom/server\");\nconst react_router_1 = __webpack_require__(/*! react-router */ \"react-router\");\nconst MainPage_1 = __webpack_require__(/*! ./pages/MainPage */ \"./src/pages/MainPage.tsx\");\nconst routes_1 = __webpack_require__(/*! ./routes */ \"./src/routes.tsx\");\nconst app = express();\napp.set('port', process.env.PORT || 3000);\nconst env = \"development\" || 'development';\nif ('development' === env) {\n    app.use(errorHandler());\n}\napp.use(express.static(path.join(__dirname, 'public')));\napp.use((req, res, next) => {\n    const content = ReactDOMServer.renderToString(React.createElement(react_router_1.StaticRouter, { location: req.url, context: {} },\n        React.createElement(routes_1.Routes, null)));\n    res.send(`<!DOCTYPE html>\\r\\n${ReactDOMServer.renderToString(React.createElement(MainPage_1.default, { content: content }))}`);\n});\nhttp.createServer(app).listen(app.get('port'), () => {\n    console.log('Express server listening on port ' + app.get('port'));\n});\n\n\n//# sourceURL=webpack:///./src/server.tsx?");

/***/ }),

/***/ "./src/services/patientsky.http.ts":
/*!*****************************************!*\
  !*** ./src/services/patientsky.http.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n// here I'll write the http server\nObject.defineProperty(exports, \"__esModule\", { value: true });\nconst axios_1 = __webpack_require__(/*! axios */ \"axios\");\nfunction getMedicine(name) {\n    return axios_1.default.get('https://fest-searcher.herokuapp.com/api/fest/s/' + name)\n        .then(function (response) {\n        if (response && response.data) {\n            return response.data;\n        }\n        else {\n            throw Error(\"Data is missing\");\n        }\n    })\n        .then(function (data) {\n        return data;\n    })\n        .catch(function (error) {\n        console.log(error);\n    });\n}\nexports.getMedicine = getMedicine;\n\n\n//# sourceURL=webpack:///./src/services/patientsky.http.ts?");

/***/ }),

/***/ "./src/utils.ts":
/*!**********************!*\
  !*** ./src/utils.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\nObject.defineProperty(exports, \"__esModule\", { value: true });\nfunction findWhere(array_object, partial_object) {\n    let ret_object = {\n        i: -1,\n        object: null\n    };\n    array_object.some((object, i) => {\n        for (const key in object) {\n            if (object.hasOwnProperty(key)) {\n                const element = object[key];\n                for (const _key in partial_object) {\n                    if (partial_object.hasOwnProperty(_key)) {\n                        if (object[key] === partial_object[_key]) {\n                            ret_object = {\n                                object: object,\n                                i: i\n                            };\n                            return (object[key] === partial_object[_key]);\n                        }\n                    }\n                }\n            }\n        }\n    });\n    return ret_object;\n}\nexports.findWhere = findWhere;\nfunction formatBirthday(bday) {\n    if (bday.length < 8) {\n        throw Error(\"The length of the bday should be 8\");\n    }\n    return `${bday.substring(0, 2)}/${bday.substring(2, 4)}/${bday.substring(4, 8)}`;\n}\nexports.formatBirthday = formatBirthday;\n\n\n//# sourceURL=webpack:///./src/utils.ts?");

/***/ }),

/***/ 0:
/*!******************************!*\
  !*** multi ./src/server.tsx ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! /Users/mca/dev/patientsky-challange/src/server.tsx */\"./src/server.tsx\");\n\n\n//# sourceURL=webpack:///multi_./src/server.tsx?");

/***/ }),

/***/ "autobind-decorator":
/*!*************************************!*\
  !*** external "autobind-decorator" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"autobind-decorator\");\n\n//# sourceURL=webpack:///external_%22autobind-decorator%22?");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"axios\");\n\n//# sourceURL=webpack:///external_%22axios%22?");

/***/ }),

/***/ "errorhandler":
/*!*******************************!*\
  !*** external "errorhandler" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"errorhandler\");\n\n//# sourceURL=webpack:///external_%22errorhandler%22?");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n\n//# sourceURL=webpack:///external_%22express%22?");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n\n//# sourceURL=webpack:///external_%22http%22?");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n\n//# sourceURL=webpack:///external_%22path%22?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react\");\n\n//# sourceURL=webpack:///external_%22react%22?");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-dom/server\");\n\n//# sourceURL=webpack:///external_%22react-dom/server%22?");

/***/ }),

/***/ "react-router":
/*!*******************************!*\
  !*** external "react-router" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router\");\n\n//# sourceURL=webpack:///external_%22react-router%22?");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"react-router-dom\");\n\n//# sourceURL=webpack:///external_%22react-router-dom%22?");

/***/ })

/******/ });
});