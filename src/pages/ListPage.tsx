import * as React from 'react';
import { Component } from "react";
import autobind from 'autobind-decorator';

import { PatientList } from "../components/PatientList";
import { PatientAddForm } from "../components/PatientAddForm";
import { patients, IPatient } from "../const";
import { findWhere } from "../utils";

export class ListPage extends Component<any, any> {

    componentDidMount() {
        this.setState({
            patients: patients,
            edit_patient: null
        })
    }

    @autobind
    addPatient(patient: IPatient) {
        let { i } = findWhere(this.state.patients, { email: patient.email });

        if (i === -1) { // ADD
            this.state.patients.push(patient);
        } else {
            this.state.patients[i] = patient
        }

        this.setState({
            patients: this.state.patients,
            edit_patient: null 
        })

    } 

    @autobind
    onEdit(email: string) {
        let { object } = findWhere(this.state.patients, { email: email});
        console.log('patient', object)
        this.setState({
            edit_patient: object
        });
    }

    @autobind
    onDelete(email: string) {
        let { i } = findWhere(this.state.patients, { email: email});
        this.state.patients.splice(i, 1);
        this.setState({
            patients: this.state.patients
        });
    }

    render() {
        return <div>
            <h2>Patient List</h2>
            <section> 
                { (this.state && this.state.patients) ? <PatientList patients={ this.state.patients } onEdit={ this.onEdit } onDelete={ this.onDelete } /> : null }
            </section>
            <h3>Add Patient</h3>
            <section>
                <PatientAddForm onAddPatient={ this.addPatient } initPatient={ (this.state && this.state.edit_patient) ? this.state.edit_patient : null } />
            </section>
        </div>
    }
}