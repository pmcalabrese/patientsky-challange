/**
 * Contains constants and interfaces
 */

 export interface IAreaOfUse {
    label: string,
    value: string
 }

 export interface IDosis {
    label: string,
    value: string
 }

 export interface IPackage {
    id: string,
    name: string,
    packageSize: string,
    units: string,
    type: string
 }

export interface IMedicine {
    _id: string,
    id: string,
    typeName: string,
    typeId: string,
    atcId: string,
    atcCatName: string,
    atcName: string,
    substanceName: string,
    productName: string,
    form: string,
    strength: string,
    units: string,
    packages: IPackage[],
    areaOfUse: IAreaOfUse[],
    dosis: IAreaOfUse[]
}

export interface IPatient {
    name: string,
    email: string,
    birthday: string,
    phone: string,
    medicines?: IMedicine[]
}

export const patients: IPatient[] = [
    {
        name: "Marco",
        email: "pm.calabrese@gmail.com",
        birthday: "12021986",
        phone: "+4542838188"
    },
    {
        name: "Gianlu",
        email: "gia@gmail.com",
        birthday: "05031986",
        phone: "+454233181"
    },
    {
        name: "Frank",
        email: "frank@gmail.com",
        birthday: "10011985",
        phone: "+4542238122"
    },
    {
        name: "Gianvito",
        email: "gi@gmail.com",
        birthday: "03091992",
        phone: "+4512348189"
    }
]