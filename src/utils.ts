export function findWhere<T, K>(array_object: Array<T>, partial_object: any): { i: number, object: T | null} {
    let ret_object = {
        i: -1,
        object: null
    };
    array_object.some((object, i) => {
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                const element = object[key];
                for (const _key in partial_object) {
                    if (partial_object.hasOwnProperty(_key)) {
                        if (object[key] === partial_object[_key]) {
                            ret_object = {
                                object: object,
                                i: i
                            };
                            return(object[key] === partial_object[_key])
                        }
                    }
                }
            }
        }
    })
    return ret_object;
}

export function formatBirthday(bday: string) {
    if (bday.length < 8) {
        throw Error("The length of the bday should be 8")
    }
    return `${bday.substring(0,2)}/${bday.substring(2,4)}/${bday.substring(4,8)}`
}