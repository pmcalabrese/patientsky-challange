/* tslint:disable:no-unused-variable */
import * as React from 'react';
/* tslint:disable:no-unused-variable */

import { Component } from "react";
import autobind from 'autobind-decorator'

/**
 * IMPORT CONSTANTS
 */

import { IPatient, patients } from "../const";

interface PatientAddFormProp {
    initPatient?: IPatient
    onAddPatient?: (patient: IPatient) => void
}

interface PatientAddFormState {
    patient: IPatient | null,
    edit_mode: boolean
}


export class PatientAddForm extends Component<PatientAddFormProp, PatientAddFormState> {

    inputs = {
        $name_input: null,
        $email_input: null,
        $birthday_input: null,
        $phone_input: null
    } 

    componentDidMount() {
        this.inputs.$name_input.focus();
        if (this.props.initPatient) {
            this.setState({
                patient: this.props.initPatient
            })
        } 
    }

    componentWillReceiveProps(props) {
        if (props.initPatient) {
            for (const input in this.inputs) {
                if (this.inputs.hasOwnProperty(input)) {
                    const $input = this.inputs[input];
                    $input.value = props.initPatient[$input.name];
                }
            }
            this.setState({
                edit_mode: true
            })
        }
    }

    @autobind
    addPatient(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        this.setState({
            patient: {
                name: this.inputs.$name_input.value,
                email: this.inputs.$email_input.value,
                birthday: this.inputs.$birthday_input.value,
                phone: this.inputs.$phone_input.value
            }
        }, () => {            
            if (this.props.onAddPatient) this.props.onAddPatient(this.state.patient)
            for (const input in this.inputs) {
                if (this.inputs.hasOwnProperty(input)) {
                    const $input = this.inputs[input];
                    $input.value = "";
                }
            }
        })
        if (this.state.edit_mode) {
            this.setState({
                edit_mode: false
            })
        }
    }

    @autobind
    handleInputChange(e: React.ChangeEvent<HTMLInputElement>) {

    }

    render() {
        return <form onSubmit={ this.addPatient }>
            <label htmlFor="">
                Name:
                <input required name="name" ref={e => this.inputs.$name_input = e} type="text" onInput={ this.handleInputChange } />
            </label>
            <br />
            <label htmlFor="">
                Email:
                <input required name="email" ref={e => this.inputs.$email_input = e} type="email" onInput={ this.handleInputChange } />
            </label>
            <br />
            <label htmlFor="">
                Birthday:
                <input required name="birthday" ref={e => this.inputs.$birthday_input = e} type="text" onInput={ this.handleInputChange } />
            </label>
            <br />
            <label htmlFor="">
                Phone:
                <input required name="phone" ref={e => this.inputs.$phone_input = e} type="text" onInput={ this.handleInputChange } />
            </label>
            <br />
            <button type="submit">{ (this.state && this.state.edit_mode) ? "Save" : "Create" } patient</button>
        </form>
    }
}