/* tslint:disable:no-unused-variable */
import * as React from 'react';
/* tslint:disable:no-unused-variable */

import { Component } from "react";
import { getMedicine } from "../services/patientsky.http";
import { IMedicine } from "../const";
import autobind from 'autobind-decorator';

interface SearchMedicineState {
    results: IMedicine[]
}

export class SearchMedicine extends Component<any, SearchMedicineState> {
    $input: HTMLInputElement = null;

    componentWillMount() {
        this.setState({ 
            results: []
        })
    }

    @autobind
    search() {
        if (this.$input.value === "") {
            this.setState({ 
                results: []
            })
        } else {
            getMedicine(this.$input.value).then((data) => {
                if (!data) return;
                this.setState({
                    results: data
                })
            });
        }
    }

    render() {
        // if (!this.state) return <span></span>
        return <div>
            <input type="text" ref={ e => this.$input = e} onChange={ this.search }/>
            <ul>
                {this.state.results.map(( result ) => {
                    return <li key={ result.id }>{ result.productName }</li>
                })}
            </ul>
        </div>
    }
}