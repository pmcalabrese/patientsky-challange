import * as React from 'react';
import { Component } from "react";

import { formatBirthday } from "../utils";

/**
 * IMPORT CONSTANTS
 */

import { patients, IPatient } from "../const";

interface PatientListProps {
    patients: IPatient[],
    onEdit?: ( email: string ) => void 
    onDelete?: ( email: string ) => void 
}

export class PatientList extends Component<PatientListProps, any> {

    render() {
        {return this.props.patients.map((patient) => {
            return <li key={ patient.email }>
                        <span>{ patient.name } </span>
                        <span><a href={`mailto:${patient.email}`}>{ patient.email }</a> </span>
                        <span>{ patient.phone } </span>
                        <span>{ formatBirthday(patient.birthday) } </span>
                        <button type="button" onClick={ e => this.props.onEdit(patient.email) }>edit</button>
                        <button type="button" onClick={ e => this.props.onDelete(patient.email) }>delete</button>
                    </li>
        })}
    }
}