// here I'll write the http server

import axios from "axios";
import { IMedicine } from "../const";

export function getMedicine(name: string) {
    return axios.get('https://fest-searcher.herokuapp.com/api/fest/s/' + name)
        .then(function (response) {
            if (response && response.data) {
                return response.data
            } else {
                throw Error("Data is missing");
            }
        })
        .then(function (data) {
            return data as IMedicine[]
        })
        .catch(function (error) {
            console.log(error);
        });
}

