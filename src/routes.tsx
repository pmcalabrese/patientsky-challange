// import "./less/style.less";
import * as React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

/**
 * IMPORT PAGES
 */

import { ListPage } from "./pages/ListPage";
import { SearchMedicinePage } from "./pages/SearchMedicinePage";

export const Routes = () => (
    <Switch>
        <Route path="/" exact component={ListPage} />
        <Route path="/search" exact component={SearchMedicinePage} />
    </Switch>
)