const path = require('path');
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var options = {
    entry: {
        app: [path.join(__dirname, "src", "index.tsx")],
    },
    output: {
        path: path.join(__dirname, "www", "public"),
        filename: '[name].js',
        libraryTarget: 'umd',
        umdNamedDefine: true
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx', ".jsx"]
    },
    plugins: [
        // new AssetsPlugin({ filename: 'www/assets.json' }),
        // new webpack.NamedModulesPlugin(),
        // new webpack.HotModuleReplacementPlugin()
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    devServer: {
        historyApiFallback: true,
        inline: true,
        hot: true,
        contentBase: "www/public"
    },
    module: {
        rules: [{
            test: /\.ts$|\.tsx$/,
            loader: 'ts-loader'
        },
        {
            test: /\.less$/,
            use: [
                {
                    loader: "css-loader",
                    options: {
                        sourceMap: true,
                        modules: true,
                        localIdentName: "[local]___[hash:base64:5]"
                    }
                },
                {
                    loader: "less-loader"
                }
            ]
        }]
    }
};

switch (process.env.NODE_ENV) {
    case "production":
        // options.output.filename = '[name].[chunkhash].min.js';
        break;
}

module.exports = options;
